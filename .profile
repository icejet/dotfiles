export QT_QPA_PLATFORMTHEME="qt5ct"
export EDITOR=/usr/bin/vim
export GTK2_RC_FILES="$HOME/.gtkrc-2.0"
# fix "xdg-open fork-bomb" export your preferred browser from here
export BROWSER=/usr/bin/surf
export READER="zathura"
export NNN_BMS="h:~/;f:~/files;c:~/files/code/c-projects"
export NNN_USE_EDITOR=1
