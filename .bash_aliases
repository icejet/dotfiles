# Install a deb package
alias dbi='sudo dpkg -i'
# Update all at once
alias up='sudo apt update && sudo apt upgrade'
# List all installed packages
alias li='dpkg-query -l'
# Search a package from cache
alias se='apt-cache search'
# quick install
alias ins='sudo apt install'
# quick shutdown
alias sdn='sudo shutdown now'
# quick reboot
alias rbn='sudo reboot'
